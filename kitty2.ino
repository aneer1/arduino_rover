#define C_TRIGGER 6
#define C_ECHO 7
#define L_TRIGGER 2
#define R_TRIGGER 4
#define L_ECHO 3
#define R_ECHO 5

#define MOTOR_ENABLE 9
#define L_MOTOR_1 10
#define L_MOTOR_2 11
#define R_MOTOR_1 12
#define R_MOTOR_2 13

#define OCCLUDED 10
#define SPEED 128

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(C_TRIGGER, OUTPUT);
  pinMode(C_ECHO, INPUT);
  pinMode(L_TRIGGER, OUTPUT);
  pinMode(L_ECHO, INPUT);
  pinMode(R_TRIGGER, OUTPUT);
  pinMode(R_ECHO, INPUT);
  pinMode(L_MOTOR_1, OUTPUT);
  pinMode(L_MOTOR_2, OUTPUT);
  pinMode(R_MOTOR_1, OUTPUT);
  pinMode(R_MOTOR_2, OUTPUT);
  pinMode(MOTOR_ENABLE, OUTPUT);
  analogWrite(MOTOR_ENABLE, SPEED);
}

void loop() {
  long r_distance, l_distance, c_distance;
  
  c_distance = distance(duration(C_TRIGGER, C_ECHO));
  l_distance = distance(duration(L_TRIGGER, L_ECHO));
  r_distance = distance(duration(R_TRIGGER, R_ECHO));
  
  if(c_distance <= OCCLUDED){
    Serial.println("backing up ");
    back_up();
  }
  else if(l_distance <= OCCLUDED){
    Serial.println("turning right");
    turn_right();
  }
  else if(r_distance <= OCCLUDED){
    Serial.println("turning left");
    turn_left();
  }
  else { 
    forward();
    Serial.println("going forward");
  }
  //Serial.println(l_distance);
  //Serial.println(r_distance);
  delay(10);
}

long duration(int trigPin, int echoPin){
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
//  delayMicroseconds(1000); - Removed this line
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  long duration = pulseIn(echoPin, HIGH);
  return duration;
}

long distance(long duration){
  long distance = (duration/2) / 29.1;
  return distance; //29.1 is the conversion from microseconds to cm
}

//reverse the direction you want to turn
void turn_left(){
  digitalWrite(R_MOTOR_1, HIGH);
  digitalWrite(R_MOTOR_2, LOW);
  
  digitalWrite(L_MOTOR_1, LOW);
  digitalWrite(L_MOTOR_2, HIGH);
}
void turn_right(){
  digitalWrite(L_MOTOR_1, HIGH);
  digitalWrite(L_MOTOR_2, LOW);
  
  digitalWrite(R_MOTOR_1, LOW);
  digitalWrite(R_MOTOR_2, HIGH); 
}
void forward(){
  digitalWrite(L_MOTOR_1, HIGH);
  digitalWrite(L_MOTOR_2, LOW);
 
  digitalWrite(R_MOTOR_1, HIGH);
  digitalWrite(R_MOTOR_2, LOW); 
}

void back_up(){
  digitalWrite(L_MOTOR_1, LOW);
  digitalWrite(L_MOTOR_2, HIGH);
 
  digitalWrite(R_MOTOR_1, LOW);
  digitalWrite(R_MOTOR_2, HIGH);
  delay(500);
  int rand = random(0,2);
  if(rand == 0){
    turn_left();
  }
  else{
    turn_right();
  }
  delay(500);
}
